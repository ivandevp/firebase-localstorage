var productos = [
    {
        'nombre': 'Taco al pastor',
        'precio':  15
    },
    {  
        'nombre': 'Agua de Horchata', 
        'precio': 20
    },
    {
        'nombre': 'Quesadillas', 
        'precio': 13
    },
    {
        'nombre': 'Tortas',
        'precio': 25
    } 
];

// var carta = ''; 

for (var i = 0; i < 4; i++) {
    // carta += (i+1) + '. ' + productos[i].nombre + ' - $' + productos[i].precio + '\n';
    var row = document.createElement('div');
    var col = document.createElement('div');
    var card = document.createElement('div');
    var cardImage = document.createElement('div');
    var cardContent = document.createElement('div');
    var cardAction = document.createElement('div');
    var image = document.createElement('img');
    var cardTitle = document.createElement('span');
    var description = document.createElement('p');
    var link = document.createElement('a');

    row.classList.add('row');
    col.className = 'col s12 m12';
    card.classList.add('card');
    cardImage.classList.add('card-image');
    cardTitle.classList.add('card-title');
    cardContent.classList.add('card-content');
    cardAction.classList.add('card-action');

    image.src = 'https://upload.wikimedia.org/wikipedia/commons/a/a2/Garnachas_del_Istmo_de_Tehuantepec.jpg';
    cardTitle.textContent = productos[i].nombre;
    description.textContent = 'Precio: $' + productos[i].precio;
    link.textContent = '+ info';

    cardImage.appendChild(image);
    cardImage.appendChild(cardTitle);
    cardContent.appendChild(description);
    cardAction.appendChild(link);

    card.appendChild(cardImage);
    card.appendChild(cardContent);
    card.appendChild(cardAction);

    col.appendChild(card);
    row.appendChild(col);

    document.getElementById('productos').appendChild(row);
}

var numeroProducto = prompt('¿Qué va a ordenar?\n\n' + carta, 'Indica el número');

var cantidad = prompt('¿Cuántos ' + productos[numeroProducto - 1].nombre.toLowerCase() +  ' quiere?');

var precioProducto = productos[numeroProducto - 1].precio;

alert('Su cuenta es ' + (cantidad * precioProducto));